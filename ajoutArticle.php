<?php session_start();?>

<!doctype html>
<html lang="en">
  <head>
    <?php include 'modules/head.php'; ?>
  </head>
  <body class="bg-light">
    <header>
    <?php include 'modules/navbar.php'; ?>
    </header>
    <div class="container">
    <div class="jumbotron p-4 p-md-5 text-white rounded bg-dark bg-5">
      <div class="col-md-6 px-0">
        <h1 class="display-4 font-italic">Nouvel Article</h1>
        <p class="lead my-3">Lorem, ipsum dolor sit, amet consectetur adipisicing elit. Excepturi itaque autem ducimus dolores ab consectetur, unde distinctio sed nulla modi! Maxime labore debitis quam illo omnis non animi obcaecati molestiae.</p>
      </div>
    </div>
  </div>
    <main>
      <div class="container">
        <div class="row">
          <aside class="col-md-4 order-md-2 mb-4 blog-sidebar">
            <div class="p-4 mb-3 bg-light rounded">
              <h4 class="font-italic">Contact-Us</h4>
              <p class="mb-0">
                  <ul>
                    <li>Tel : <?php echo(TEL)?></li>
                    <li>Address : <?php echo(ADRESSE)?></li>
                    <li><?php echo(POSTALE)?></li>
                  </ul>
              </p>
            </div>
          </aside><!-- /.blog-sidebar -->
          <div class="col-md-8 order-md-1">
            <?php 
            if(isset($_SESSION['article'])){
              if($_SESSION['article'] == true){
                echo('<div class="alert alert-success" role="alert">Article ajouté avec succès !</div>');
              }
              else{
                echo('<div class="alert alert-danger" role="alert">Erreur lors de l\'ajout de l\'article !</div>');
              }
            }
            ?>
            <h4 class="mb-3">Votre Article</h4>
            <form action="traitement_article.php" method="post" enctype="multipart/form-data">
              <div class="row">
                <div class="col-md-6 mb-3">
                  <label for="title">Titre de l'Article</label>
                  <input type="text" class="form-control" id="title" name="title">
                </div>
              </div>

              <div class="mb-3">
                <label for="content">Déscription</label>
                <textarea name="description" class="form-control" id="content" cols="30" rows="10"></textarea>
              </div>
              
              <legend>Article</legend>
                <div class="mb-3">
                <label for="article">Ajouter votre article</label>
                <textarea name="article" class="form-control" id="article_content" cols="30" rows="10"></textarea>
              </div>
              <legend>Illustration</legend>
                <div class="mb-3">
                <label for="illustration">Photo d'illustration de votre article</label>
                <input type="file" class="form-control" name="illustration">
              </div>
              <hr class="mb-4">
              <button class="btn btn-primary btn-lg btn-block" type="submit">Envoyer le message</button>
            </form>
          </div>
        </div>
        
      </div>

    </main>
    
<?php include 'modules/footer.php'; ?>
  
</body>
</html>
