<?php session_start();?>
<!doctype html>
<html lang="en">
  <head>
    <?php include 'modules/head.php'; ?>
  </head>
  <body class="bg-light">
    <header>
    <?php include 'modules/navbar.php'; ?>
    </header>
    <div class="container">
    <div class="jumbotron p-4 p-md-5 text-white rounded bg-dark bg-6">
      <div class="col-md-6 px-0">
        <h1 class="display-4 font-italic">Inscrivez-vous sur notre site</h1>
        <p class="lead my-3">Lorem, ipsum dolor sit, amet consectetur adipisicing elit. Excepturi itaque autem ducimus dolores ab consectetur, unde distinctio sed nulla modi! Maxime labore debitis quam illo omnis non animi obcaecati molestiae.</p>
      </div>
    </div>
  </div>
    <main>
      <div class="container">
        <div class="row">
          <aside class="col-md-4 order-md-2 mb-4 blog-sidebar">
            <div class="p-4 mb-3 bg-light rounded">
              <h4 class="font-italic">Contact-us</h4>
              <p class="mb-0">
                  <ul>
                    <li>Tel : <?php echo(TEL)?></li>
                    <li>Address : <?php echo(ADRESSE)?></li>
                    <li><?php echo(POSTALE)?></li>
                  </ul>
              </p>
            </div>
          </aside><!-- /.blog-sidebar -->
          <div class="col-md-8 order-md-1">
            <?php 
              if(!isset($_SESSION['insc']) || !$_SESSION['insc']){
                echo('<p style = "color : red">Veuillez remplir les champs</p>');  
              }
              elseif($_SESSION['emailType']){
                echo('<p style = "color : red">Format du mail incorrect</p>');
              }
            ?>
            <h4 class="mb-3">Formulaire d'inscription</h4>
            <form action="ressources.php" method="post" enctype="multipart/form-data">
              <fieldset>
                <legend>Vos informations personnelles</legend>
                <div class="row">
                  <div class="col-md-6 mb-3">
                    <label for="firstName">Prénom</label>
                    <input type="text" class="form-control" id="firstName" name="firstName" required="required">
                  </div>
                  <div class="col-md-6 mb-3">
                    <label for="lastName">Nom</label>
                    <input type="text" class="form-control" id="lastName" name="lastName" required="required">
                  </div>
                </div>

                <div class="mb-3">
                  <label for="username">Adresse email</label>
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">@</span>
                    </div>
                    <input type="email" class="form-control" id="username" name="email" required="required">
                  </div>
                  <label for="inputPassword">Mot de passe</label>
                  <input type="password" id="inputPassword" class="form-control" placeholder="Password" required name ="loginMdp" required="required">
                </div>
              </fieldset>
              
              <fieldset>
                <legend>Vos passions</legend>
                <div class="custom-control custom-checkbox">
                  <input type="checkbox" class="custom-control-input" id="hobbie-ski" name="hobbie[]" value="Ski">
                  <label class="custom-control-label" for="hobbie-ski">
                    Le ski
                  </label>
                </div>
                <div class="custom-control custom-checkbox">
                  <input type="checkbox" class="custom-control-input" id="hobbie-roller" name="hobbie[]" value="Roller">
                  <label class="custom-control-label" for="hobbie-roller">
                    Le roller
                  </label>
                </div>
                <div class="custom-control custom-checkbox">
                  <input type="checkbox" class="custom-control-input" id="hobbie-foot" name="hobbie[]" value="Foot">
                  <label class="custom-control-label" for="hobbie-foot">
                    Le foot
                  </label>
                </div>
                <div class="custom-control custom-checkbox">
                  <input type="checkbox" class="custom-control-input" id="hobbie-basket" name="hobbie[]" value="Basket">
                  <label class="custom-control-label" for="hobbie-basket">
                    Le basket
                  </label>
                </div>
              </fieldset>
              <fieldset>
                <legend>Votre avatar</legend>
                <div class="mb-3">
                <label for="avatar">Choisissez une photo</label>
                <input type="file" class="form-control" name="file">
              </div>
              </fieldset>
              <button class="btn btn-primary btn-lg btn-block" type="submit">Inscription</button>
            </form>
          </div>
        </div>
        
      </div>

    </main>
    
<?php include 'modules/footer.php'; ?>
  
</body>
</html>
