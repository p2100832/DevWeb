<?php
include ("./include/pdo.php");
session_start();
if(isset($_POST['email']) && isset($_POST['firstName']) && isset($_POST['lastName']) && isset($_POST['loginMdp'])){
    if(filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){
        $_SESSION['emailType'] = false;
        header("Location: ./inscription.php");
    }
    $_SESSION['insc'] = true;
    $new_password = password_hash($_POST['loginMdp'],PASSWORD_DEFAULT);
    $count = $pdo->exec("INSERT INTO users (email,name,lastname,password) VALUES ('" . $_POST['email'] . "','" . $_POST['firstName'] . "','" . $_POST['lastName'] . "','" . $new_password . "');");
    header("Location: ./login.php");
}
else{
    $_SESSION['insc'] = false;
    header("Location: ./inscription.php");
}
?>
<!doctype html>
<html lang="en">
    <head>
        <?php include  'modules/head.php'; ?>
    </head>
    <body>
        <?php include  'modules/navbar.php'; ?> 
        <table>
            <thead>
                <tr>
                    <th colspan="2">Infos Récupérées</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Prénom :</td>
                    <td><?php if(isset ($_POST['firstName'])) echo($_POST['firstName'])?></td>
                </tr>
                <tr>
                    <td>Nom :</td>
                    <td><?php if(isset ($_POST['lastName'])) echo($_POST['lastName'])?></td>
                </tr>
                <tr>
                    <td>Mail :</td>
                    <td><?php if(isset ($_POST['email'])) echo($_POST['email'])?></td>
                </tr>
                <tr>
                    <td>Passions :</td>
                    <td>
                        <?php getHobbies()?>
                    </td>
                </tr>
                <tr>
                    <td>Photo :</td>
                    <td>
                        <?php displayFile() ?>
                    </td>
                </tr>
            </tbody>
        </table>
        <?php include  'modules/footer.php'; ?>
    </body>
</html>