<?php session_start();?>
<!doctype html>
<html lang="fr">
  <head>
    <?php include 'modules/head.php'; ?>
  </head>
  <body class="text-center">
    <header>
      <?php include 'modules/navbar.php'; ?>
    </header>
    <form class="form-signin" action="traitement.php" method="post" enctype="multipart/form-data">
      <h1 class="h3 mb-3 font-weight-normal">Formulaire de connexion</h1>

      <?php 
        if(!isset($_SESSION['log']) || !$_SESSION['log']){
          echo('<p style = "color : red">Mauvais identifiants</p>');  
        }
      ?>

      <label for="inputEmail">Adresse Email</label>
      <input type="email" id="inputEmail" class="form-control" placeholder="Email address" required autofocus name ="loginEmail">

      <label for="inputPassword">Mot de passe</label>
      <input type="password" id="inputPassword" class="form-control" placeholder="Password" required name ="loginMdp">

      <div class="checkbox mb-3">
        <label>
          <input type="checkbox" value="remember-me" name = "remember"> Se souvenir de moi
        </label>
      </div>
      <button class="btn btn-lg btn-primary btn-block" type="submit">Connexion</button>
      <p class="mt-5 mb-3 text-muted">&copy; 2017-2020</p>
    </form>
    <?php include 'modules/footer.php'; ?>
</body>
</html>
