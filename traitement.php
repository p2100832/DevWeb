<?php 
include 'include/pdo.php';
session_start();
if(isset($_POST['loginEmail']) and isset($_POST['loginMdp'])){
    if(filter_var($_POST['loginEmail'], FILTER_VALIDATE_EMAIL)){
        $_SESSION['log'] = false;
        $login = $_POST['loginEmail'];
        $password = $_POST['loginMdp'];
        /*$sql = "SELECT * FROM users WHERE email = $log";
        $row = $pdo->query($sql)->fetchAll();*/
        $statement = $pdo->prepare("SELECT * FROM users WHERE email = :varLog");
        $statement->execute(
            [
                'varLog' => $login
            ]
        );
        $row = $statement->fetch();
        if($row != false){
            if(password_verify($password,$row['password'])){
                if(!empty($_POST['remember'])){
                    setcookie("login",$login,time() + 3600);
                    setcookie("password",$password,time() + 3600);
                }
                $_SESSION['log'] = true;
                $_SESSION['email'] = $login;
                $_SESSION['name'] = $row['name'];
                $_SESSION['lastname'] = $row['lastname'];
                header("Location: ./index.php",TRUE,301);
            }
            else{
                if(!isset($_POST['remember'])){
                    setcookie("login", "", time() - 3600, "/");
                    setcookie("password", "", time() - 3600, "/");
                }
                $_SESSION['log'] = false;
                header("Location: ./login.php",TRUE,301);
        
            }
        }
        else{
            if(!isset($_POST['remember'])){
                setcookie("login", "", time() - 3600, "/");
                setcookie("password", "", time() - 3600, "/");
            }
            $_SESSION['log'] = false;
            header("Location: ./login.php",TRUE,301);
    
        }
    }
    else{
        header("Location: ./login.php",TRUE,301);
    }
}
else{
    header("Location: ./login.php",TRUE,301);
}
?>
<!doctype html>
<html lang="fr">
    <head>
    </head>
    <body class="text-center">
        <header>
        </header>
    </body>
</html>