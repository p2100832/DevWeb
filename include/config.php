    <?php 
    const TEL = "0102030405";
    const ADRESSE = "123 Rue de La Croix";
    const POSTALE = "01000 Bourg-en-Bresse";

    function debug($var){
        echo("<pre>");
        var_dump($var);
        echo("</pre>");
        die();
    };

    function getActive($filename){
        if($_SERVER['PHP_SELF'] == $filename){
            return('class="active"');
        }
    };

    function getImage(){
        if(preg_match('/avif/', $_SERVER["HTTP_ACCEPT"])){
            echo('.avif');
        }
        elseif(preg_match('/webp/', $_SERVER["HTTP_ACCEPT"])){
            echo('.webp');
        }
        else{
            echo('.jpg');
        }
    }

    function getHobbies(){
        if(isset($_POST['hobbie'])){
            $_hobbie = $_POST['hobbie'];
            if(empty($_hobbie)) 
            {
                echo("You didn't select any hobbies.");
            } 
            else
            {
                $N = count($_hobbie);
                echo("You selected $N hobbie(s): ");
                for($i=0; $i < $N; $i++)
                {
                echo($_hobbie[$i] . ",");
                }
            }
        }
    }

    function displayFile(){
        if(!empty($_FILES['file']['name'])){
            $target_dir = "uploads/avatar/";
            $target_file = $target_dir . basename($_FILES["file"]["name"]);
            $uploadOk = 1; 
            $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION)); //Check if it's a real image
            $check = getimagesize($_FILES["file"]["tmp_name"]);
            if($check !== false) {
                $uploadOk = 1;
            } else {
                $uploadOk = 0;
            }
            if($uploadOk == 1){
                move_uploaded_file($_FILES["file"]["tmp_name"], $target_file);
                echo('<img src="' . $target_file . '" alt="File upload">');
            }
        }
    }

    function isAdmin(){
        if(!isset($_SESSION['log']) || !$_SESSION['log']){
            echo('<a href="/login.php"' . getActive('/login.php') . '>Connexion</a>');
        }
        else{
            if($_SESSION['log']){
                echo('<a>Bienvenue ' . $_SESSION['name'] . ' ' . $_SESSION['lastname'] . '</a>');
            }
        }
    }

    function isConnected(){
        if(isset($_SESSION['log'])){
            if($_SESSION['log']){
                echo('<a href="/logout.php">Deconnexion</a>');
            }   
        }
    }